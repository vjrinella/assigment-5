#include "header.h"
struct FootballGameStats {
    Side winner;
    FootballTeamStats homeStats;
    FootballTeamStats visitorsStats;
};
class FootballGame {
private:
    // TODO
public:
    FootballGame(const string &homeName, const string &visitorsName);
    ~FootballGame();
    void addPlayer(Side side, const string &name);
    void recordScore(Side side, const string& playerName, int points);
    FootballGameStats getStats() const;
};