#include "header.h"
class FootballPlayer {
private:
    // TODO
public:
    FootballPlayer(const string &name);
    const string &getName() const;
    int getPointsScored() const;
    void recordScore(int points);
};