#include "header.h"
class FootballTeam {
private:
    // TODO
public:
    FootballTeam(const string &name);
    ~FootballTeam();
    void addPlayer(const string &name);
    FootballPlayer *getPlayer(const string &name) const;
};
struct FootballTeamStats {
    int score;
    string mvpName;
    int mvpScore;
};


